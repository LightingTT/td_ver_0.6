TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    game.cpp \
    map.cpp \
    gameobject.cpp \
    enemies.cpp \
    player.cpp \
    tower.cpp \
    gui.cpp
INCLUDEPATH += C:\SFML_QT\include
DEPENDPATH += C:\SFML_QT\include

LIBS += -LC:\SFML_QT\BUILD\Libs\

CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-system-d -lsfml-network-d -lsfml-main-d -lsfml-window-d -lsfml-graphics-d
CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-system -lsfml-network -lsfml-main -lsfml-window -lsfml-graphics

HEADERS += \
    game.h \
    map.h \
    gameobject.h \
    enemies.h \
    player.h \
    tower.h \
    gui.h
