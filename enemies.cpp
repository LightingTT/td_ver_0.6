#include "enemies.h"

Enemies::Enemies()
{
    position = sf::Vector2f(50, 50);
    targetPoint = sf::Vector2f(400, 50);
    targetPoint_2 = sf::Vector2f(400, 200);
    targetPoint_3 = sf::Vector2f(600, 200);

    if (!texture.loadFromFile("drake.png"))
    {
        cout<<"nope.";
    }
    texture.setSmooth(true);

    sprite.setTexture(texture);
    sprite.scale(50.f / texture.getSize().x, 50.f / texture.getSize().y);

}

void Enemies::update(const sf::Time &deltaTime)
{
    //    if (position.x < targetPoint.x)
    //    {
    //        velocity.x += accel;
    //    }
    //    if (position.x >= targetPoint.x)
    //    {
    //        velocity.y += accel;
    //    }

    //    if(velocity.x < -maxspeed)
    //        velocity.x = -maxspeed;
    //    if(velocity.x >  maxspeed)
    //        velocity.x =  maxspeed;
    //    if(velocity.y < -maxspeed)
    //        velocity.y = -maxspeed;
    //    if(velocity.y >  maxspeed)
    //        velocity.y =  maxspeed;

    //    position += velocity;

    if (position.x < targetPoint.x)
        position = sf::Vector2f(position.x + 100 * deltaTime.asSeconds(), position.y);
    if (position.x >= targetPoint.x)
        position = sf::Vector2f(position.x, position.y + 100 * deltaTime.asSeconds());
    if (position.x == targetPoint_2.x && position.y == targetPoint_2.y)
        position = sf::Vector2f(position.x + 100 * deltaTime.asSeconds(), position.y);
}

//    if (position.x < targetPoint_2.x)
//    {
//        position = sf::Vector2f(position.x + 100 * deltaTime.asSeconds(), position.y);
//    }



void Enemies::draw()
{
    sprite.setPosition(position);
    Game::getGameWindow()->draw(sprite);
}


