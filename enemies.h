#ifndef ENEMIES_H
#define ENEMIES_H
#include <SFML/Graphics.hpp>
#include "gameobject.h"
#include "game.h"

class Enemies : public GameObject
{
public:
    Enemies();

    virtual void update(const sf::Time &deltaTime);
    virtual void draw();

private:
    sf::Vector2f position;
    sf::Vector2f velocity;
    float maxspeed = 3.0f;
    float accel = 0.2f;
    float decel = 0.1f;
    sf::Vector2f targetPoint;
    sf::Vector2f targetPoint_2;
    sf::Vector2f targetPoint_3;
    sf::Vector2f targetPoint_4;
    sf::Texture texture;
    sf::Sprite sprite;
};

#endif // ENEMIES_H
