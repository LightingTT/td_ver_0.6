#include "game.h"

std::vector<GameObject*> Game::gameObjects = {};
sf::RenderWindow *Game::gameWindow = new sf::RenderWindow(sf::VideoMode(windowWidth, windowHeight), "TD");

Game::Game()
{

}
bool Game::loop()
{
    init();
    while (Game::gameWindow->isOpen())
    {
        deltaTime = clock.restart();
        events();
        update();
        render();
    }
}
void Game::init()
{
    Game::gameWindow->setFramerateLimit(framerate);
    gameMap = new Map(rows, columns);
    gameMap->createMap();
    color = sf::Color(220,220,220);
    Game::gameObjects.push_back(new gui());
}
void Game::update()
{
    if (Game::gameObjects.size() != 0) {          // iteruje sie zaraz po narysowaniu obiektu i updatuje jego pozycje, stan itp.
        for (GameObject* obj : Game::gameObjects) {
            obj->update(deltaTime);
        }
    }
}
void Game::render()
{
    Game::gameWindow->clear(color);

    for (GameObject* obj : Game::gameObjects) {   // iteruje po vectorze i kaze rysowac sie obiektom.
        obj->draw();
    }

    gameMap->displayMap();
    Game::gameWindow->display();
}

void Game::addObjectToList(GameObject *obj)
{
    Game::gameObjects.push_back(obj);
}
void Game::events()
{
    sf::Event event;
    while (Game::gameWindow->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            Game::gameWindow->close();
        if (event.type  == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Space)
        {
            Game::addObjectToList(new Enemies);
        }
        if(event.type == sf::Event::MouseButtonPressed && event.key.code == sf::Mouse::Left)
        {
            Game::addObjectToList(new Tower);
            std::cout <<gameObjects.size();
        }
        if (event.type == sf::Event::MouseMoved)
        {
             cout<<sf::Mouse::getPosition(*Game::gameWindow).x << ", " << sf::Mouse::getPosition(*Game::gameWindow).y << ")\n\n";
        }
}
}
sf::RenderWindow* Game::getGameWindow()
{
    return Game::gameWindow;
}
