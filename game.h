#ifndef GAME_H
#define GAME_H
#include <iostream>
#include <SFML/Graphics.hpp>
#include "map.h"
#include "gameobject.h"
#include "enemies.h"
#include "player.h"
#include <windows.h>
#include <time.h>
#include "tower.h"
#include "gui.h"

using namespace std;
class  Tower;
class Map;
class Enemies;
class Game
{
public:
    static const int SIZE_X = 20;
    static const int SIZE_Y = 20;
    static const int rows = 30;
    static const int columns = 40;

    Game();
    Game(int rows, int columns);

    bool loop();
    void events();
    void render();
    void update();

    void init(); // Initializes things at the beginning, once.
    static void addObjectToList(GameObject* obj); // statycznie dodaje do listy nowe obiekty
    static sf::RenderWindow *getGameWindow();

private:
    static const int windowWidth = 800;
    static const int windowHeight = 600;
    static const int framerate = 60;
    static sf::RenderWindow* gameWindow;

    Enemies* enemy;
    Map* gameMap;

    sf::Color color;
    sf::Time deltaTime;
    sf::Clock clock;

    static std::vector<GameObject*> gameObjects;// static, because we can refer to it from anywhere.
};

#endif // GAME_H
