#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SFML/Graphics.hpp>

class GameObject
{
public:
    GameObject();

    virtual void update(const sf::Time& deltaTime) = 0;
    virtual void draw() = 0;
};

#endif // GAMEOBJECT_H
