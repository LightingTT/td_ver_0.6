#include "gui.h"

gui::gui()
{
    font.loadFromFile("OpenSans-Regular.ttf");
}
void gui::update(const sf::Time &deltaTime)
{
    fps = 1.0 / deltaTime.asSeconds();
}

void gui::draw()
{
    sf::Text text(std::string(std::to_string(fps)), font);
    text.setCharacterSize(20);
    text.setColor(sf::Color::Black);
    text.setPosition(sf::Vector2f(700,500));
    Game::getGameWindow()->draw(text);
}
