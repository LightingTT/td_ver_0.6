#ifndef GUI_H
#define GUI_H
#include "gameobject.h"
#include "game.h"
class gui : public GameObject
{
public:
    gui();

    virtual void update(const sf::Time& deltaTime);
    virtual void draw();
private:
    sf::Font font;
    float fps;
};

#endif // GUI_H
