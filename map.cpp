#include "map.h"

Map::Map() : rows(rows), columns(columns)
{

}
Map::Map(int rows, int columns)
    : rows(rows), columns(columns)
{
    for (int i=0; i<rows; i++)
    {
        vector<int> map;
        for (int j=0; j<columns; j++)
        {
            map.push_back(0);
        }
        twoDimensionMap.push_back(map);
    }

}
void Map::createMap()
{
    twoDimensionMap.clear();
    texturePiece = sf::Texture();
    if(!texturePiece.loadFromFile("tree.png"))
    {
        std::cout<<"Texture loading FAIL.";
    }
    texturePiece.setSmooth(true);
    spritePiece = sf::Sprite();
    spritePiece.setTexture(texturePiece);


    for (int i=0; i<rows; i++)
    {
        vector<int> map;
        for (int j=0; j<columns; j++)
        {
            if (i==0 || i==rows-1 || j==0 || j==columns-1)
            {
                map.push_back(1);
            }
            else
                map.push_back(0);
        }
        twoDimensionMap.push_back(map);
    }

}
void Map::displayMap()
{
    int x = -1;
    int y = -1;
    for (vector<int> i : twoDimensionMap)
    {
        x++;
        y = -1;
        for (int j : i)
        {
            y++;
            if(j==1)
            {
                spritePiece.setPosition(sf::Vector2f(y*Game::SIZE_Y, x*Game::SIZE_X));
                Game::getGameWindow()->draw(spritePiece);
            }
        }
    }
}

void Map::getMapValue()
{
    std::cout<<(twoDimensionMap[20])[20];
}
