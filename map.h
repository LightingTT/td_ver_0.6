#ifndef MAP_H
#define MAP_H
#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include "game.h"

using namespace std;

class Map
{
public:

    Map();
    Map(int rows, int columns);

    void createRandMap();
    void createMap();
    void displayMap();
    void getMapValue();
private:

    vector <vector <int> > twoDimensionMap;

    int rows;
    int columns;

    sf::RectangleShape mapPiece; // for  creating rectangle pieces.
    sf::Sprite spritePiece;
    sf::Texture texturePiece;
};

#endif // MAP_H
