#ifndef PLAYER_H
#define PLAYER_H
#include "game.h"
#include "map.h"


class Player : public GameObject
{
public:
    Player();
    float getMousePos();
    void draw();
    void update();
private:
    sf::CircleShape circle;
};

#endif // PLAYER_H
