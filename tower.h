#ifndef TOWER_H
#define TOWER_H
#include <gameobject.h>
#include "game.h"

class Tower : public GameObject
{
public:
    Tower();
    void update(const sf::Time& deltaTime);
    void draw();
private:
    sf::Vector2f tower_position;
    sf::Vector2i mousePosition;
    sf::RectangleShape tower;
    sf::Texture texture;
    sf::Sprite sprite1;
};

#endif // TOWER_H
